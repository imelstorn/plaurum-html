$(document).ready(function(){

	var mob = 0;


	$('body').addClass('js');


	$(".control").blur(function () {
	    if ($(this).val() != '') {
	        $(this).addClass('not-empty');
	    } else {
	    	$(this).removeClass('not-empty');
	    }
	});


	$(".js-openMainMenu").on("click", function() {
		$(".header").toggleClass("menu-opened");
		$(".js-openMainMenu").toggleClass("opened");
		/*$('.header-menu-open').fadeToggle("fast");*/
		return false;
	});


	$(".header-menu__list .hassub:has(ul) > a").on("click", function() {
		if ($(window).width() < 1025) {
			$(this).parents(".hassub").toggleClass("opened");
			return false;
		}
	});


	if ($(window).width() < 749) {
		$(".footer-menu_has-sub dt").on("click", function() {
			$(this).nextAll("dd").toggle();
			return false;
		});
	}

	function getWinSize() {
		var a = $(window).width();
		if (a > 1025) {
			mob = 0;
		} else {
			mob = 1;
		}

	}

	getWinSize();

	$(window).resize(function() {
		getWinSize();
		console.log(mob);
	});




	$(".map-link-id").hover(
		function() {
			var id = $(this).attr("data-target");
			$(".map-v2__map-marker").not(".clicked").hide();
			$(".map-v2__map-marker_" + id).fadeIn();
		},
		function() {
			$(".map-v2__map-marker").not(".clicked").hide();
		}
	);

	$(".map-link-id").on("click", function() {
		var id = $(this).attr("data-target");
		$(".map-v2__link").removeClass("map-v2__link_checked");
		$(this).find(".map-v2__link").addClass("map-v2__link_checked");
		$(".map-v2__map-marker").removeClass("clicked").hide();
		$(".map-v2__map-marker_" + id).addClass("clicked").fadeIn();
		$(".map-v2__map-popup").hide();
		$(".map-v2__map-popup_" + id).fadeIn();
		return false;
	});

	$(".map-v2__popup_cross").on("click", function() {
		$(this).closest(".map-v2__map-popup").fadeOut();
		var id = $(this).closest(".map-v2__map-popup").attr("data-popup");
		$(".map-v2__map-marker_" + id).removeClass("clicked").fadeOut();
		return false;
	});

	$("#groups-list a").on("click", function() {
		var id = $(this).closest("li").index()*1 + 1;
		$(".groups-list__link").removeClass("groups-list__link_checked");
		$(this).addClass("groups-list__link_checked");
		$(".groups-company").hide();
		$(".groups-company-" + id).show();;
		return false;
	});

	var incLi = 0;

	$(".map-v3__pin_list li").each(function() {
		$(this).attr("data-item-src", incLi);
		incLi++;
	});

	$(".map-v3__pin").on("click", function() {
		var i = $(this).find("li").attr("data-item-src");
		//var i = $(this).closest(".map-v3__pin").index();

		$(".map-popup").hide();
		$(".map-popup").eq(i).show();
		return false;
	});



	$(".map-v3__pin_list li").on("click", function() {
		var i = $(this).attr("data-item-src");
		//var i = $(this).closest(".map-v3__pin").index();

		$(".map-popup").hide();
		$(".map-popup").eq(i).show();
		return false;
	});

	$(".map-popup__cross").on("click", function() {
		$(".map-popup").hide();
		return false;
	});

	$(".map-v3-mobile__item_title").on("click", function() {
		$(this).next().toggle();
		return false;
	});

});

$(function () {

	$(".geo-map__pin_list li").on("click", function() {
		var i = $(this).attr("data-item-src");

		$(".map-popup").hide();
		$(".map-popup").eq(i).show();

    var pin = $(this).closest('.geo-map__pin');
    var win = $(".map-popup").eq(i).find('.map-popup__item');
    var pos = pin.position();
    var size = {
      width: win.width(),
      height: win.height()
    };

    if (['1', '2', '3', '14'].indexOf(i) !== -1) {
      win.css({
        top: pos.top - size.height / 2,
        left: pos.left - size.width + pin.width()
      });
    } else {
      win.css({
        top: pos.top - size.height / 2,
        left: pos.left
      });
    };

		return false;
	});

	$(".map-popup__cross").on("click", function() {
		$(".map-popup").hide();
		return false;
	});

	$(".geo-map-mobile__item_title").on("click", function() {
		$(this).next().toggle();
		return false;
	});

	$(".map-v3-mobile__item_title").on("click", function() {
		$(this).find(".plus").toggle();
		$(this).find(".minus").toggle();	
		return false;
	});

});
